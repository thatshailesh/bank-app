import accounts from "./data/accounts.json";

export default function getAccounts() {
  return accounts;
}
