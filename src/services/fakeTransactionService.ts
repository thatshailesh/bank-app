import { TransactionList } from "components/types/TransactionList";
import { Transaction } from "components/types/Transaction";

export async function getTransactionDetails(
  accountId: number,
  transactionId: number
): Promise<Transaction[]> {
  const result = await import(`./data/transactions-${accountId}.json`);
  const details = result.default;

  return details.filter((item: any) => item.id === transactionId);
}

export async function getTransactionList(
  accountId: number
): Promise<TransactionList[]> {
  const result = await import(`./data/transactions-${accountId}.json`);
  const list = [...result.default];
  return list.map((el) => {
    const { id, transaction_date, amount, transaction_processed } = el;
    return {
      id,
      transaction_date,
      amount,
      transaction_processed,
    };
  });
}
