import React, { useEffect, useState } from "react";
import { BrowserRouter } from "react-router-dom";
import AccountList from "components/Accounts/List";
import getAccounts from "services/fakeAccountService";
import { Account as IAccount } from "components/types/Account";

function App() {
  const [accounts, setAccounts] = useState<IAccount[]>([]);
  const [error, setError] = useState(null);
  const fetchAccounts = async () => {
    try {
      setAccounts((await getAccounts()) as any);
    } catch (err) {
      setError(err);
    }
  };
  useEffect(() => {
    fetchAccounts();
  }, []);

  if (error) {
    return (
      <div className="alert alert-danger">
        <h3>{error.message ? error.message : "Something went wrong"}</h3>
      </div>
    );
  }
  return (
    <BrowserRouter>
      <AccountList accounts={accounts} />
    </BrowserRouter>
  );
}

export default App;
