import React from "react";
import { render, waitForElement } from "@testing-library/react";
import App from "../App";

it("renders without crashing", async () => {
  await waitForElement(() => render(<App />));
});
