import React from "react";

import {
  render,
  fireEvent,
  waitForElement,
  screen,
  within,
} from "@testing-library/react";
import Accounts from "components/Accounts/List";
import Transactions from "components/Transactions/TransactionList";
import TransactionDetails from "components/Transactions/TransactionDetails";
import { mount } from "enzyme";
import { act } from "react-dom/test-utils";
import {
  getTransactionList,
  getTransactionDetails,
} from "../services/fakeTransactionService";

describe("Accounts", () => {
  describe("Unit Tests", () => {
    const accounts = [
      {
        id: 1,
        account_name: "Savings Account",
        account_type: "savings",
        balance: 32334.32,
        currency: "SGD",
        account_number: "342423455344",
        is_active: true,
      },
    ];
    it("finds first account name", async () => {
      const { container } = render(<Accounts accounts={accounts} />);
      expect(container.children.length).toBe(1);
    });

    it("selects savings account and renders transaction", async () => {
      const { getByTestId, getByText } = render(
        <Accounts accounts={accounts} />
      );
      fireEvent.click(getByTestId("item-0"));
      const elem = await waitForElement(() => getByTestId("item-0"));
      expect(elem.classList[0]).toBe("active");

      const transactions = await getTransactionList(accounts[0].id);
      const keys = Object.keys(transactions[0]);
      keys.map((el) => {
        const row = screen.getByText(el.toUpperCase()).closest("tr");
        const utils = within(row);
        expect(utils.getByText(el.toUpperCase())).toBeInTheDocument();
      });
    });

    it("selects transaction", async () => {
      const transactionsList = await getTransactionList(accounts[0].id);
      const jestMock = jest.fn();
      const { getByTestId } = render(
        <Transactions
          transactionList={transactionsList}
          onTransactionSelect={jestMock}
        />
      );
      fireEvent.click(getByTestId("item-0"));
      expect(jestMock).toBeCalledTimes(1);
    });

    it("renders transaction details", async () => {
      const transactionDetails = await getTransactionDetails(accounts[0].id, 1);
      const { getByTestId, getByText } = render(
        <TransactionDetails transactions={transactionDetails} isVertical />
      );
      const elem = await waitForElement(() => getByTestId("item-0"));
      const keys = Object.keys(transactionDetails[0]);
      keys.map((el) => {
        const row = screen.getByText(el.toUpperCase()).closest("tr");
        const utils = within(row);
        expect(utils.getByText(el.toUpperCase())).toBeInTheDocument();
      });
    });
  });

  describe.only("Snanshot Tests", () => {
    it("renders correctly when there are no accounts", () => {
      const tree = mount(<Accounts accounts={[]} />);
      expect(tree).toMatchSnapshot();
    });

    it("renders correctly when there are multiple accounts", () => {
      const accounts = [
        {
          id: 1,
          account_name: "Savings Account",
          account_type: "savings",
          balance: 32334.32,
          currency: "SGD",
          account_number: "342423455344",
          is_active: true,
        },
        {
          id: 2,
          account_name: "Bonus Savings Account",
          account_type: "savings",
          balance: -5.11,
          currency: "AUD",
          account_number: "156478245",
          is_active: true,
        },
      ];
      const tree = mount(<Accounts accounts={accounts} />);
      expect(tree).toMatchSnapshot();
    });

    it("renders transaction list", () => {
      const transactions = [
        {
          id: 1,
          account_id: 1,
          description:
            "Dolore quis ad et mollit nisi excepteur ex anim fugiat quis ipsum exercitation proident cupidatat. Quis anim incididunt excepteur cupidatat aliquip nulla reprehenderit velit. Dolor pariatur velit consectetur dolore aliquip reprehenderit non aliqua consectetur. Sunt aliquip consequat et in eu aute.\r\n",
          from: "Savings Account - 342423455344",
          transaction_date: "2019-06-08T03:37:28 -08:00",
          transaction_processed: true,
          amount: "$1,373.41",
        },
      ];
      const component = mount(
        <Transactions
          transactionList={transactions}
          onTransactionSelect={jest.fn}
        />
      );

      expect(component).toMatchSnapshot();
    });

    it("renders transaction details", () => {
      const transactions = [
        {
          id: 1,
          account_id: 1,
          description:
            "Dolore quis ad et mollit nisi excepteur ex anim fugiat quis ipsum exercitation proident cupidatat. Quis anim incididunt excepteur cupidatat aliquip nulla reprehenderit velit. Dolor pariatur velit consectetur dolore aliquip reprehenderit non aliqua consectetur. Sunt aliquip consequat et in eu aute.\r\n",
          from: "Savings Account - 342423455344",
          transaction_date: "2019-06-08T03:37:28 -08:00",
          transaction_processed: true,
          amount: "$1,373.41",
        },
      ];
      const component = mount(
        <TransactionDetails transactions={transactions} isVertical />
      );
      expect(component).toMatchSnapshot();
    });
  });
});
