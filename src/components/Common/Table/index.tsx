import React from "react";

interface TableProps {
  items: any[];
  onItemSelect?(transactionId: number): void;
  isVertical: boolean;
}
interface RenderRowProps {
  keys: string[];
  item: any;
}

const RenderRow = (props: RenderRowProps) => {
  const { item, keys } = props;
  const rowList = keys.map((key, index) => {
    return <td key={item[key]}>{String(item[key])}</td>;
  });
  return <>{rowList}</>;
};

const Table: React.SFC<TableProps> = (props) => {
  const { items, onItemSelect, isVertical } = props;
  const getKeys = () => {
    return Object.keys(items[0]);
  };

  const getHeader = () => {
    if (isVertical) return [];
    const keys = getKeys();
    return keys.map((el) => <th key={el}>{el.toUpperCase()}</th>);
  };

  const getRowsData = () => {
    const keys = getKeys();
    if (isVertical) {
      const data = items[0];
      const rowList = keys.map((col, index) => {
        return (
          <tr key={col} data-testid={`item-${index}`}>
            <th>{col.toUpperCase()}</th>
            <td>{data[col]}</td>
          </tr>
        );
      });
      return <>{rowList}</>;
    }
    return items.map((row, index) => {
      return (
        <tr
          data-testid={`item-${index}`}
          key={row.id}
          onClick={() => (onItemSelect ? onItemSelect(row.id) : {})}
          role="button"
        >
          <RenderRow item={row} keys={keys} />
        </tr>
      );
    });
  };

  return (
    <table className="table">
      <thead>
        <tr>{getHeader()}</tr>
      </thead>
      <tbody>{getRowsData()}</tbody>
    </table>
  );
};

export default Table;
