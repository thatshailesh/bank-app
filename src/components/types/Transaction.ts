export interface Transaction {
  id: number;
  account_id: number;
  description: string;
  from: string;
  transaction_date: string;
  transaction_processed: boolean;
  amount: string;
}
