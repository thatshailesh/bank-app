export interface TransactionList {
  id: number;
  transaction_date: string;
  amount: string;
  transaction_processed: boolean;
}
