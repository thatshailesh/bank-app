export interface Account {
  id: number;
  account_name: string;
  account_type: string;
  balance: number;
  currency: string;
  account_number: string;
  is_active: boolean;
}
