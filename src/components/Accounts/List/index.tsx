import React, { useState, useEffect } from "react";
import { TransactionList } from "components/types/TransactionList";
import { Transaction } from "components/types/Transaction";
import {
  getTransactionList,
  getTransactionDetails,
} from "services/fakeTransactionService";
import Transactions from "components/Transactions/TransactionList";
import TransactionDetails from "components/Transactions/TransactionDetails";
import AccountsListEl from "../ListElement";
import { Account } from "../../types/Account";

export interface AccountListProps {
  accounts: Account[];
}

const AccountList: React.SFC<AccountListProps> = (props) => {
  const { accounts } = props;
  const [selectedIdx, setIdx] = useState(-1);
  const [transactionList, setTransactions] = useState<TransactionList[]>([]);
  const [transactionDetails, setTransactionDetails] = useState<Transaction[]>(
    []
  );
  const [transactionId, setTransactionId] = useState<number>(-1);
  const [error, setError] = useState(null);

  const generateKey = (pre: number) => {
    return `${pre}_${new Date().getTime()}`;
  };

  const handleAccountSelect = (idx: number) => {
    setIdx(idx);
    setTransactionDetails([]);
    setTransactionId(-1);
  };

  const handleTransactionSelect = (trId: number) => {
    setTransactionId(trId);
  };

  useEffect(() => {
    async function fetchTransactionList(accountId: number) {
      try {
        setTransactions(await getTransactionList(accountId));
      } catch (err) {
        setError(err);
      }
    }
    const accountId =
      accounts.length && selectedIdx > -1
        ? accounts[selectedIdx].id
        : undefined;
    if (accountId !== undefined) {
      fetchTransactionList(accountId);
    }
  }, [accounts, selectedIdx]);

  useEffect(() => {
    async function fetchTransactionDetails(accountId: number) {
      try {
        setTransactionDetails(
          await getTransactionDetails(accountId, transactionId)
        );
      } catch (err) {
        setError(err);
      }
    }
    const accountId =
      accounts.length && selectedIdx > -1
        ? accounts[selectedIdx].id
        : undefined;
    if (accountId !== undefined && transactionId > 0) {
      fetchTransactionDetails(accountId);
    }
  }, [accounts, selectedIdx, transactionId, transactionList.length]);

  const accountList = accounts.map((el: Account, idx: number) => (
    <AccountsListEl
      testId={`item-${idx}`}
      key={generateKey(el.id)}
      account_name={el.account_name}
      account_number={el.account_number}
      balance={el.balance}
      currency={el.currency}
      isAccountActive={String(el.is_active)}
      onAccountSelect={() => handleAccountSelect(idx)}
      active={selectedIdx === idx}
    />
  ));

  if (error) {
    return (
      <div className="alert alert-danger">
        <h3>{error.message ? error.message : "Something went wrong"}</h3>
      </div>
    );
  }

  return (
    <>
      <div className="container">
        <div className="d-flex w-100 flex-column">
          <h1>Accounts</h1>

          <p>You have {accounts.length} accounts</p>
        </div>
        {accountList}
        <div className="wrapper">
          {transactionList.length > 0 ? (
            <Transactions
              transactionList={transactionList}
              onTransactionSelect={handleTransactionSelect}
            />
          ) : (
            []
          )}
          {transactionDetails.length > 0 ? (
            <TransactionDetails transactions={transactionDetails} isVertical />
          ) : (
            []
          )}
        </div>
      </div>
    </>
  );
};

export default AccountList;
