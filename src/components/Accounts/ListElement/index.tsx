import React from "react";

export interface AccountListElProps {
  account_name: string;
  account_number: string;
  balance: number;
  active: boolean;
  currency: string;
  isAccountActive: string;
  testId: string;
  onAccountSelect(event: React.MouseEvent<HTMLDivElement, MouseEvent>): void;
}

const AccountListEl: React.SFC<AccountListElProps> = (props) => {
  const {
    account_name,
    account_number,
    balance,
    currency,
    onAccountSelect,
    active,
    isAccountActive,
    testId,
  } = props;
  const accountDetails = {
    accountName: account_name,
    accountNumber: account_number,
    balance,
    currency,
  };
  return (
    <div
      data-testid={testId}
      className={active ? "active list-group-item" : "list-group-item"}
      onClick={onAccountSelect}
      role="button"
    >
      <div className="d-flex w-100">
        <h5 className="mb-1">{account_name}</h5>
      </div>
      <div className="d-flex w-100">
        <p className="mb-1">Account Number: {account_number}</p>
      </div>
      <div className="d-flex w-100">
        <p className="mb-1">Balance: {`${currency} ${balance}`}</p>
      </div>
      <div className="d-flex w-100">
        <small className="text-muted">Active: {isAccountActive}</small>
      </div>
    </div>
  );
};

export default AccountListEl;
