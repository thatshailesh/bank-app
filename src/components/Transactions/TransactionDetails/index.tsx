import React from "react";
import { Transaction } from "components/types/Transaction";
import Table from "../../Common/Table";

export interface TransactionDetailsProps {
  transactions: Transaction[];
  isVertical: boolean;
}

const TransactionDetails: React.SFC<TransactionDetailsProps> = (props) => {
  const { transactions, isVertical } = props;
  return (
    <div className="flex-2">
      <Table items={transactions} isVertical />
    </div>
  );
};

export default TransactionDetails;
