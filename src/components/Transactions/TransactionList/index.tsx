import { TransactionList } from "components/types/TransactionList";

import React from "react";
import Table from "../../Common/Table";

export interface TransactionsProps {
  transactionList: TransactionList[];
  onTransactionSelect(id: number): void;
}

const Transactions: React.SFC<TransactionsProps> = (props) => {
  const { transactionList, onTransactionSelect } = props;

  return (
    <div className="flex-1">
      <Table
        items={transactionList}
        onItemSelect={onTransactionSelect}
        isVertical={false}
      />
    </div>
  );
};

export default Transactions;
